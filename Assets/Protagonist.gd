class_name Player
extends CharacterBody2D

@export var speed := 200
@export var run_speed := 250
@export var jump_speed := 900

signal i_died

var vel := Vector2.ZERO
var broke_block := false
var alive := true
var jumping := false
var is_shroomed := false


func get_input():
	vel.x = 0
	var move_speed = speed
	if Input.is_action_pressed("Action"):
		move_speed = run_speed
	
	if Input.is_action_pressed("Right"):
		vel.x += move_speed
	if Input.is_action_pressed("Left"):
		vel.x -= move_speed


func break_block():
	vel.y = -abs(vel.y)
	broke_block = true


func _physics_process(delta):
	if Global.level_ending:
		vel.x = speed
	else:
		get_input()
	
	vel.y += Global.gravity * delta
	set_velocity(vel)
	set_up_direction(Vector2.UP)
	move_and_slide()
	vel = velocity
	
	if Input.is_action_just_pressed("Jump") or Input.is_action_just_pressed("Up"):
		if is_on_floor() and !jumping and !Global.level_ending:
			jumping = true
			vel.y = -jump_speed
			broke_block = false


func bounce():
	vel.y = -abs(-jump_speed / 2.0)


func fall():
	die()


func power_up():
	scale.y = 1.5
	is_shroomed = true


func die():
	if is_shroomed:
		is_shroomed = false
		scale.y = 1
	else:
		alive = false
		queue_free()
		emit_signal("i_died")


func _on_I_Died_body_entered(body):
	if body.alive:
		die()


func _on_My_feet_body_entered(_body):
	Global.protagonist_on_ground()
	jumping = false
