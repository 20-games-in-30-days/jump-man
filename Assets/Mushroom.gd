extends CharacterBody2D

@export var speed := 50
@export var is_one_up := false
var vel := Vector2.ZERO
var x_direction = 1
var active := false


func _ready():
	$AnimationPlayer.play("Rise")

var alive = true


func _physics_process(delta):
	if active:
		vel.x = speed * x_direction
		vel.y += Global.gravity * delta
		set_velocity(vel)
		set_up_direction(Vector2.UP)
		move_and_slide()
		vel = velocity
		if is_on_wall():
			x_direction = -x_direction


func _on_Area_body_entered(body):
	if active:
		if is_one_up:
			Global.lives += 1
		else:
			body.power_up()
		queue_free()


func _on_AnimationPlayer_animation_finished(_anim_name):
	active = true
